PHP, MongoDB geospatial query example
=====================================

This code demonstrates how to use a vanilla MongoDB installation to create a collection of objects and search them using a '2dsphere' index.

In the example, I am creating a directory of services for health related organisations.

This is only a basic demonstration of how to hang things together - I hope it helps.

Stuff used and referenced:

- PHP: http://php.net
- MongoDB: https://www.mongodb.com
- PHP Mongo extension: http://php.net/manual/en/book.mongo.php
- MongoDB 2dsphere index: http://docs.mongodb.org/manual/tutorial/query-a-2dsphere-index/