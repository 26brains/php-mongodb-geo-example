<?php
/**
 * DirectoryService is the base class for each of the main services
 */
class DirectoryService 
{
	public $typeOf;
	public $name;
	public $address;
	public $location;

	function __construct(){
		$this->typeOf = get_class($this);
	}
}

interface Describer{
	public function describeMe($append);
}

/**
 * DirectoryService is the base class for each of the main services
 */
class Pharmacist extends DirectoryService implements Describer
{
	public $typeOf;
	public $minorAliment;

	public function describeMe($append){
		$description = "I am ".$this->name.", a ".__CLASS__.". I ";
		if($this->minorAliment){
			$description .= "have";
		}else{
			$description .= "do not have";
		}
		$description .= " a minor ailemnt unit.".$append."\n\n";
		return $description;
	}
}

/**
 * DirectoryService is the base class for each of the main services
 */
class Hospital extends DirectoryService implements Describer
{
	public $typeOf;
	public $hasAE;

	public function describeMe($append){
		$description = "I am ".$this->name.", a ".__CLASS__.". I ";
		if($this->hasAE){
			$description .= "provide";
		}else{
			$description .= "do not provide";
		}
		$description .= " A&E services.".$append."\n\n";
		return $description;
	}
}

/**
 * DirectoryServicesFactory is the base class for each of the main services
 */
class DirectoryServicesFactory{
	static function createFromArray($arr){
		if(!is_array($arr) || !class_exists($arr['typeOf'])){
			return;
		}
		$service = new $arr['typeOf'];
		foreach($arr as $key => $val){
			if($key == '_id' || $key == 'typeOf'){
				continue;
			}
			$service->$key = $val;
		}
		return $service;
	}
}