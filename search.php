<?php
include_once __DIR__."/Geo.php";
include_once __DIR__."/DirectoryServices.php";

// connect
$m = new MongoClient();

// select a database
$db = $m->dlsaccess;

// select a collection (analogous to a relational database's table)
$collection = $db->directoryServices;

//center point is DLS London
$lat = 51.504388;
$long = -0.255926;

//search radius is (in metres):
$distance=100000;

//This is the MongoDB
$query = array( 'location' =>
	array("\$near" =>
		array("\$geometry" => 
			array('type'=>'Point','coordinates'=>
				array($long,$lat)),"\$minDistance" => 0, "\$maxDistance" => $distance)
		)
	);

//This runs the query
$cursor = $collection->find($query);

// iterate through the results
foreach ($cursor as $document) {
    if($s = DirectoryServicesFactory::createFromArray($document)){
    	print $s->describeMe(" I am ".round(Geo::distance($s->location[1],$s->location[0],$lat,$long,"K"),2)."km from your search point.");
    }
}
