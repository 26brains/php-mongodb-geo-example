<?php
include __DIR__."/DirectoryServices.php";

/**
 * Fixtures
 *
 * If this was a relational database, we would need a table with columns like:
 *
 * name | address | location | minor_ailment | has_ae
 *
 * A row of data would only use the columns that are relevant to it. That is less than ideal and
 * is unhelpful if we want to store fields for which we do not have a column.
 *
 * Enter MongoDB, a NoSQL database...
 *
 * Mongo stores objects. It needs to have indexes to find things, so *some* columns need to be 
 * consistent, but anything else goes. So, in the example below we store Pharmacies that have a
 * minor ailment flag and hospitals that have a has A&E (accident and emergency) flag.
 */

$services = array();

$tmp = new Pharmacist();
$tmp->name = "Boots";
$tmp->minorAliment = true;
$tmp->location = array(-0.478270, 51.546668);

$services[] = $tmp;

$tmp = new Hospital();
$tmp->name = "Great Ormand Street";
$tmp->hasAE = false;
$tmp->location = array(-0.1207834, 51.5225023);

$services[] = $tmp;

$tmp = new Hospital();
$tmp->name = "Hillingdon Hosiptal";
$tmp->hasAE = true;
$tmp->location = array(-0.461132, 51.526867);

$services[] = $tmp;

$tmp = new Hospital();
$tmp->name = "Sir John Radcliffe";
$tmp->hasAE = false;
$tmp->location = array(-1.217959, 51.764095);

$services[] = $tmp;



// connect
$m = new MongoClient();

// select a database
$db = $m->dlsaccess;

// select a collection (analogous to a relational database's table)
$collection = $db->directoryServices;

//empty previous collection, if needed
$collection->remove(array());

// add fixtures to collection
$collection->batchInsert($services);

// add index - this is needed for geospatial searching
$collection->ensureIndex(array("location" => "2dsphere"));